(defproject gephi-pairs "0.1.0"
  :description "GEPHI Pairs: Generate file of input node pairs to GEPHI"
  :dependencies [ [org.clojure/clojure "1.2.1"]
                  [org.clojure/clojure-contrib "1.2.0"] ]
  :main gpairs.core
  :uberjar-name gephi-pairs.jar
)
